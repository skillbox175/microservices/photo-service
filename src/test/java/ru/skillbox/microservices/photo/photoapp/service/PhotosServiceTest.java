package ru.skillbox.microservices.photo.photoapp.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AnonymousAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import io.findify.s3mock.S3Mock;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.microservices.photo.photoapp.properties.S3Properties;
import ru.skillbox.microservices.photo.photoapp.repository.PhotosRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Slf4j
@ExtendWith(MockitoExtension.class)
class PhotosServiceTest {

    @Mock
    S3Properties properties;
    @Mock
    MultipartFile photo;

    AmazonS3Client client;
    S3Mock api;
    PhotosRepository repository;
    PhotosService photosService;
    private final String bucketName = "my-bucket";
    private final String photoFileName = "some-wonderful-photo.jpg";

    @BeforeEach
    void initMocks() throws IOException {
        this.api = new S3Mock.Builder().withPort(8888).withInMemoryBackend().build();
        api.start();

        AwsClientBuilder.EndpointConfiguration endpoint = new AwsClientBuilder.EndpointConfiguration("http://localhost:8888", "ru-region");
        this.client = (AmazonS3Client) AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(new AWSStaticCredentialsProvider(new AnonymousAWSCredentials()))
                .build();
        Mockito.when(properties.getBucketPhotos()).thenReturn(bucketName);
        Mockito.lenient().when(photo.getInputStream()).thenReturn(new ByteArrayInputStream("some file".getBytes()));
        this.repository = new PhotosRepository(client,properties);
        this.photosService = new PhotosService(repository,properties);
    }

    @AfterEach
    void tearDown() {
        api.shutdown();
    }
    @Test
    void uploadPhotosSuccessful() throws IOException {
        client.createBucket(bucketName);
        String result = photosService.uploadPhoto(photoFileName, photo);
        Assertions.assertEquals("Photo 'some-wonderful-photo.jpg' was successfully uploaded", result);
    }

    @Test
    void uploadPhotosException() throws IOException {
        String result = photosService.uploadPhoto(photoFileName, photo);
        Assertions.assertEquals("Photo 'some-wonderful-photo.jpg' have not uploaded", result);
    }

    @Test
    void deletePhotoSuccessful() throws IOException {
        client.createBucket(bucketName);
        photosService.uploadPhoto(photoFileName, photo);
        var result = photosService.deletePhoto(photoFileName);
        Assertions.assertEquals("Photo 'some-wonderful-photo.jpg' was successfully deleted", result);
    }

    @Test
    void deletePhotoException(){
        client.createBucket(bucketName);
        var result = photosService.deletePhoto(photoFileName);
        Assertions.assertEquals("Photo 'some-wonderful-photo.jpg' have not deleted", result);
    }
}
