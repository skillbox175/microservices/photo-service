package ru.skillbox.microservices.photo.photoapp.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.microservices.photo.photoapp.properties.S3Properties;
import ru.skillbox.microservices.photo.photoapp.repository.PhotosRepository;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhotosService {

    private final PhotosRepository photosRepository;

    private final S3Properties properties;

    @NewSpan
    public String uploadPhoto(String fileName,MultipartFile file) throws IOException {
        log.info("Uploading {} to bucket {} in S3", fileName ,properties.getBucketPhotos());
        try (InputStream stream = file.getInputStream()) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("");
            metadata.setContentLength(file.getSize());
            photosRepository.put(fileName, stream, metadata);
        } catch (AmazonServiceException e) {
            log.error(e.getErrorMessage());
            return String.format("Photo '%s' have not uploaded", fileName);
        }
        log.info("Photo '{}' was successfully uploaded", fileName);
        return String.format("Photo '%s' was successfully uploaded", fileName);
    }

    @NewSpan
    public String deletePhoto(String fileName) {
        String bucketName = properties.getBucketPhotos();
        log.info("Removing {} from bucket {} in S3", fileName, bucketName);
        if (photosRepository.listKeys(fileName).isEmpty()) {
            return String.format("Photo '%s' have not deleted", fileName);
        }
        photosRepository.delete(fileName);

        log.info("Photo '{}' was successfully deleted", fileName);
        return String.format("Photo '%s' was successfully deleted", fileName);
    }
}
