package ru.skillbox.microservices.photo.photoapp.repository;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.sleuth.annotation.NewSpan;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor
public class S3Repository {
    private final AmazonS3 s3Client;
    private final String bucketName;

    @NewSpan
    public Collection<String> listKeys(String prefix) {
        return s3Client.listObjectsV2(bucketName, prefix)
                .getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .toList();
    }

    @NewSpan
    public Collection<S3ObjectSummary> listObjects(String prefix) {
        return s3Client.listObjectsV2(bucketName, prefix)
                .getObjectSummaries();
    }

    @NewSpan
    public void delete(String key) {
        s3Client.deleteObject(bucketName, key);
    }

    @NewSpan
    public void put(String key, InputStream inputStream, ObjectMetadata metadata) {
        s3Client.putObject(bucketName, key, inputStream, metadata);
    }

    @NewSpan
    public Optional<S3Object> get(String key) {
        try {
            return Optional.of(s3Client.getObject(bucketName, key));
        } catch (AmazonServiceException exception) {
            return Optional.empty();
        }
    }
}
