package ru.skillbox.microservices.photo.photoapp.repository;

import com.amazonaws.services.s3.AmazonS3;
import ru.skillbox.microservices.photo.photoapp.properties.S3Properties;
import org.springframework.stereotype.Component;

@Component
public class PhotosRepository extends S3Repository {
    public PhotosRepository(AmazonS3 s3Client, S3Properties properties) {
        super(s3Client, properties.getBucketPhotos());
    }
}
