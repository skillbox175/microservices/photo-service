package ru.skillbox.microservices.photo.photoapp.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.skillbox.microservices.photo.photoapp.service.PhotosService;

import java.io.IOException;

@Slf4j
@RestController
//@ControllerAdvice
@RequiredArgsConstructor
@RequestMapping(value = "/apiv1/photos")
//@MultipartConfig
public class PhotosController {
    private final PhotosService photosService;

    @PostMapping(value = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public String uploadPhoto(@RequestParam("image") MultipartFile photo) throws IOException {
        String photoFileName = photo.getOriginalFilename();
        log.info("S3 post event received for {}", photoFileName);
        return photosService.uploadPhoto(photoFileName, photo);
    }

    @DeleteMapping("/delete")
    public String deletePhoto(@RequestParam("fileName") String fileName) throws IOException {
        log.info("S3 remove event received for {}", fileName);
        return photosService.deletePhoto(fileName);
    }

    @GetMapping("")
    public String helloController() {
        log.info("Hello event happened");
        return "Hello from photos service!";
        }
}