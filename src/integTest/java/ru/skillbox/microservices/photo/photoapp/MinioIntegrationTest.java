package ru.skillbox.microservices.photo.photoapp;

import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@ActiveProfiles("integTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MinioIntegrationTest {
    private static final int MINIO_PORT = 9000;
    private static final String MINIO_ROOT_USER = "minio_admin";
    private static final String MINIO_ROOT_PASSWORD = "minio_admin";

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Container
    private static final GenericContainer minioServer = new GenericContainer("bitnami/minio")
            .withExposedPorts(MINIO_PORT)
            .withEnv("MINIO_ROOT_USER", MINIO_ROOT_USER)
            .withEnv("MINIO_ROOT_PASSWORD", MINIO_ROOT_PASSWORD)
            .waitingFor(Wait.forLogMessage(".*Documentation: https://min.io/docs/minio/linux/index.html.*",1));

    @DynamicPropertySource
    static void registerPgProperties(DynamicPropertyRegistry registry) {
        int port = minioServer.getMappedPort(MINIO_PORT);
        registry.add("aws.s3.access-key", () -> MINIO_ROOT_USER);
        registry.add("aws.s3.secret-key", () -> MINIO_ROOT_PASSWORD);
        registry.add("aws.s3.endpoint", () -> "http://" + minioServer.getHost() + ":" + port + "/");
    }
    @BeforeEach
    public void init(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void uploadPhotoSuccessful() throws Exception {
        assertThat(minioServer.isRunning()).isTrue();
        log.info(minioServer.execInContainer("mc", "mb", "local/photos-bucket").getStdout());

        MockMultipartFile testFile = new MockMultipartFile(
                "image",
                "test.jpg",
                "image/jpg",
                "dummy image1".getBytes());

        ResultActions resultActions = mockMvc.perform(
                multipart("/apiv1/photos/upload").file(testFile));
        resultActions.andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.equalTo("Photo 'test.jpg' was successfully uploaded")));
    }
}

